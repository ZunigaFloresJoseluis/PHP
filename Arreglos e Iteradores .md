# Arreglos e Iteradores
## Arreglos

Un arreglo es un mapa ordenado. puedes hacer un arreglo de la siguientes maneras;

```php
$arreglo = [];
$arreglo = array();
```
Un arreglo con valores establecidos es declarado como:

```php
$arreglo = ['a','b'];

$arreglo = array(a => 102, b => 103); // a partir de php 5
```
Un arreglo es asociativo cuando contiene una llave  y el conector "*=>*"

Para agregar elementos en el arreglo en PHP puede utilizar:
```php
$arreglo_example = [a => 1, b => 2, c => 3];

$arreglo_example[]=12

```
```php


```



#### Funciones de Arreglos

Las función *array_combine()* es una función que crea un arreglo al combinar dos arreglos

```php
$valores = ['hola','como'];
$valores01 = ['mundo','es'];

$arreglo = array_combine($valores,$valores01)

```
    El resultado es:
    
    ['hola','como','mundo','es'];
    
La función *array_values()* devuelve un arreglo de los valores sin las llaves, y *array_keys()* devuelve un arreglo de todas la claves del arreglo. La funcion *array_flip()* devuelve un arreglo
con las llaves y valores invertidos.

```php
$arreglo_example = [a => 1, b => 2, c => 3];

$arreglo = array_values(arreglo_example);
$arreglo = array_keys(arreglo_example);
$arreglo = array_flip(arreglo_example);


```
##### List

Al igual que Array(), List no es una función si no un constructor del lenguaje, se utiliza para asignar una lista de variables a valores de una arreglo.
    
A continuacion se muestra un ejemplo de uso de las List().
    
```php
$arreglo = ['verde','guacamole'];
$list($color,$comida) = $arreglo;//se declaran las variables para utilizarlas despues
echo "Mi comida favorita es $comida, y es de color $color;

```
Por otro lado tambien se puede agregar variables a indices distintos.

```php
$arreglo = ['verde','guacamole'];
$list($comida[1],$color[0]) = $arreglo;//se declaran las variables para utilizarlas despues
echo "Mi comida favorita es $comida, y es de color $color;

```
List funciona para mapas tambien, cuando no se detalla la variable en un indice este no se ve reflejado en ninguna variable, puede que solo queramos utilizar
la primera y ultima variable esto haria que dejemos ese espacio sin asignar a ninguna variable.

```php
$foo = array(2 => 'a', 'foo' => 'b', 0 => 'c');
$foo[1] = 'd';
list($x, $y, $z) = $foo;
var_dump($foo, $x, $y, $z);
```
