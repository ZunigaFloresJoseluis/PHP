# Sintaxis en PHP

## Creando un archivo php

En PHP es regular ver codigo donde se mezcla etiquetas de html de la siguiente manera

```php
<div> ... </div>
<?php  
    if($_GET['error'] == True){
        echo "<p> error</p>";
        
    }

?>

```
Por otro lado solo puede tener instrucciones en lenguaje  PHP. Como se puede observar no se necesita terminar  con *?>* ya que
esto se hace cuando se mezcla con html o cuando se desea delimitar el codigo y no tal cuando solamente se tienen ordenes en PHP.
```php
<?php
   if($_GET['error'] == True){
        echo "<p> error</p>";
        
    }


```


Estos archivos para ser reconocidos por el lenguaje de php deben contar con la extensión: **.php** ademas de que cada instrucción debe estar
separada por un **;** 

### Comentarios en PHP

En PHP se pueden utilizar argumentos como en cualquier otro lenguaje esto se hace de esta manera
```php
// Comentario de una sola linea

/*
 Comentario de  multiple linea
*/

```

## Tipos de Datos en PHP

Los tipos de datos soportados en PHP son:

* Booleano
* Numericos
* Decimales
* Cadenas
* Arrays
* Iterables
* Objetos
* Nulo
*  ** Recurso **

### Boolean 

Este es el mas sencillo tipo de dato. Se utilizan en las condicionales.
Se puede definir una variable de tipo bool en php como
 ```php
    $tipo_bool = True;
    $tipo_bool01 = False;
    
```

En php  cualquier valor diferente a 0 es true como por ejemplo 

* False
* Array con 0 elementos
* String vacio
* Integer 0 
* Float 0.0
* Null

### Integers

Los integer en PHP se pueden declarar en diferentes bases hexadecimal, decimal , Octal, Binario, Negativos 
```php

$a = 1234; // número decimal
$a = -123; // un número negativo
$a = 0123; // número octal (equivale a 83 decimal)
$a = 0x1A; // número hexadecimal (equivale a 26 decimal)
$a = 0b11111111; // número binario (equivale al 255 decimal)

```


### Floats 

Los tipos Float en PHP tambien llamados de punto flotante, se definen de esta manera.

```php
$flotante = 2.3;
$flotante = 7E-2
$flotante = 1.2e3; 
```
    Advertencia:
    Los números de punto flotante tienen una precisión limitada. 
    Aunque depende del sistema, PHP típicamente utiliza el formato de doble precisión IEEE 754,
    el cual dará un error relativo máximo por aproximación del orden de 1.11e-16.
    
### String (Cadenas)

En PHP se puede tratar de estas formas:
* Encomillado simple 
* Encomillado doble 
* Heredoc
    

Comillas simples
```php
$str= 'Ejemplo';
$str_01= ')='
```

Comillas dobles
```php
$str = "Ejemplo cadenas dobles";
```
Heredoc
```php
$str = <<<'EOD'
 Ejemplo de string con Heredoc
EOD;


```
Variables Staticas en clase

```php
static $bar = <<<LABEL
Nada aquí...
LABEL;
```

Variables constantes en clase
Se define una constante en PHP con la palabra reservada *const*
```php
 const BAR = <<<FOOBAR
Ejemplo de constante
FOOBAR;
```

Arreglos de String 

```php
$jugos = array("manzana", "naranja", "koolaid1" );

```
