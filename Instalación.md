# Instalación 


![Linux](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/NewTux.svg/300px-NewTux.svg.png)
# Linux

Para instalar PHP en la distribución de linux que te encuentres en muchas distribuciones 
se cuenta con un comando de instalacion y descarga de paquetes  ya sea pkg, pacman, zypper.

Puede que se puedan instalar con el paquete de LAMP
* Linux
* PHP
* Mysql
* Apache

Por otro lado puedes descargar los archivos de php desde su página [PHP sitio](http://php.net/downloads.php)

Al descargar el archivo con extención .tar  debemos encontrar descomprimir  
    
    $ cd $HOME/Downloads/
    $ tar xvf php.7.tar.gz
    $ cd php

Puedes instalarlo con el comando cmake y despues ejecutando make.
    
    $ cmake 
    $ make 

Para  corroborar la existencia de PHP en windows desde el CMD puedes ejecutar.
    
    php -v

Teniendo como resultado 
>PHP 7.2.2 (cli) (built: Jan 31 2018 19:51:55) ( ZTS MSVC15 (Visual C++ 2017) x86 )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies



![Windows](https://www.sysadminsdecuba.com/wp-content/uploads/2017/10/6165-windows-550x381.png)
# Windows

En el entorno de windows existe  algunos programas que ofrece las caracteristicas de LAMP como Xamp o Wamp
estos programas tiene ya determinadamente mysql o mariadb  junto con php y un servidor como apache 

Otra forma de instalar PHP en windows es primero
1. Descargar desde la pagina oficial [PHP-Descarga](https://windows.php.net/download#php-7.2)
2. Descomprimir el archivo en algun lugar  donde no sea eliminado 
3. Agregar a la variable del sistema PATH la direccion del directorio del archivo.



Para  corroborar la existencia de PHP en windows desde el CMD puedes ejecutar.
    
    php -v

Teniendo como resultado 
>PHP 7.2.2 (cli) (built: Jan 31 2018 19:51:55) ( ZTS MSVC15 (Visual C++ 2017) x86 )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies